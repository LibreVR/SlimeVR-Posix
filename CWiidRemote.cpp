#include "CWiidRemote.h"
#include <unistd.h>

void cwiid_callback(cwiid_wiimote_t *wiimote, int mesg_count,
                    union cwiid_mesg mesg[], struct timespec *timestamp)
{
	printf("cwiid_callback TODO\n");
}

void set_led_state(cwiid_wiimote_t *wiimote, unsigned char led_state)
{
	if (cwiid_set_led(wiimote, led_state)) {
		fprintf(stderr, "Error setting LEDs \n");
	}
}

void set_rpt_mode(cwiid_wiimote_t *wiimote, unsigned char rpt_mode)
{
	if (cwiid_set_rpt_mode(wiimote, rpt_mode)) {
		fprintf(stderr, "Error setting report mode\n");
	}
}

void print_state(struct cwiid_state *state)
{
	int i;
	int valid_source = 0;

    #if 0
	printf("Report Mode:");
	if (state->rpt_mode & CWIID_RPT_STATUS) printf(" STATUS");
	if (state->rpt_mode & CWIID_RPT_BTN) printf(" BTN");
	if (state->rpt_mode & CWIID_RPT_ACC) printf(" ACC");
	if (state->rpt_mode & CWIID_RPT_IR) printf(" IR");
	if (state->rpt_mode & CWIID_RPT_NUNCHUK) printf(" NUNCHUK");
	if (state->rpt_mode & CWIID_RPT_CLASSIC) printf(" CLASSIC");
	if (state->rpt_mode & CWIID_RPT_BALANCE) printf(" BALANCE");
	if (state->rpt_mode & CWIID_RPT_MOTIONPLUS) printf(" MOTIONPLUS");
	printf("\n");
    #endif
	
	printf("Active LEDs:");
	if (state->led & CWIID_LED1_ON) printf(" 1");
	if (state->led & CWIID_LED2_ON) printf(" 2");
	if (state->led & CWIID_LED3_ON) printf(" 3");
	if (state->led & CWIID_LED4_ON) printf(" 4");
	printf("\n");

	printf("Rumble: %s\n", state->rumble ? "On" : "Off");

	printf("Battery: %d%%\n",
	       (int)(100.0 * state->battery / CWIID_BATTERY_MAX));

	printf("Buttons: %X\n", state->buttons);

	printf("Acc: x=%d y=%d z=%d\n", state->acc[CWIID_X], state->acc[CWIID_Y],
	       state->acc[CWIID_Z]);

	printf("IR: ");
	for (i = 0; i < CWIID_IR_SRC_COUNT; i++) {
		if (state->ir_src[i].valid) {
			valid_source = 1;
			printf("(%d,%d) ", state->ir_src[i].pos[CWIID_X],
			                   state->ir_src[i].pos[CWIID_Y]);
		}
	}
	if (!valid_source) {
		printf("no sources detected");
	}
	printf("\n");
    
    return;

	switch (state->ext_type) {
	case CWIID_EXT_NONE:
		printf("No extension\n");
		break;
	case CWIID_EXT_UNKNOWN:
		printf("Unknown extension attached\n");
		break;
	case CWIID_EXT_NUNCHUK:
		printf("Nunchuk: btns=%.2X stick=(%d,%d) acc.x=%d acc.y=%d "
		       "acc.z=%d\n", state->ext.nunchuk.buttons,
		       state->ext.nunchuk.stick[CWIID_X],
		       state->ext.nunchuk.stick[CWIID_Y],
		       state->ext.nunchuk.acc[CWIID_X],
		       state->ext.nunchuk.acc[CWIID_Y],
		       state->ext.nunchuk.acc[CWIID_Z]);
		break;
	case CWIID_EXT_CLASSIC:
		printf("Classic: btns=%.4X l_stick=(%d,%d) r_stick=(%d,%d) "
		       "l=%d r=%d\n", state->ext.classic.buttons,
		       state->ext.classic.l_stick[CWIID_X],
		       state->ext.classic.l_stick[CWIID_Y],
		       state->ext.classic.r_stick[CWIID_X],
		       state->ext.classic.r_stick[CWIID_Y],
		       state->ext.classic.l, state->ext.classic.r);
		break;
	case CWIID_EXT_BALANCE:
		printf("Balance: right_top=%d right_bottom=%d "
		       "left_top=%d left_bottom=%d\n",
		       state->ext.balance.right_top,
		       state->ext.balance.right_bottom,
		       state->ext.balance.left_top,
		       state->ext.balance.left_bottom);
		break;
	#if 0
	//TODO: upgrade cwiid
	case CWIID_EXT_MOTIONPLUS:
		printf("MotionPlus: angle_rate=(%d,%d,%d) low_speed=(%d,%d,%d)\n",
		       state->ext.motionplus.angle_rate[0],
		       state->ext.motionplus.angle_rate[1],
		       state->ext.motionplus.angle_rate[2],
		       state->ext.motionplus.low_speed[0],
		       state->ext.motionplus.low_speed[1],
		       state->ext.motionplus.low_speed[2]);
		break;
	#endif
	}
}


int CWiidRemote::open()
{
	/* Connect to the wiimote */
	
	bdaddr = {};
	printf("Put Wiimote in discoverable mode now (press 1+2)...\n");
	if (!(wiimote = cwiid_open(&bdaddr, 0))) {
		fprintf(stderr, "Unable to connect to wiimote\n");
		return -1;
	}
	if (cwiid_set_mesg_callback(wiimote, cwiid_callback)) {
		fprintf(stderr, "Unable to set message callback\n");
		return -1;
	}
    
    set_led_state(wiimote, CWIID_LED1_ON);
    set_rpt_mode(wiimote, CWIID_RPT_ACC | CWIID_RPT_BTN);
    
	return 0;
}

const struct cwiid_state& CWiidRemote::getState()
{
	if (cwiid_get_state(wiimote, &state)) {
		fprintf(stderr, "Error getting state\n");
	}
	print_state(&state);
    return state;
}

void CWiidRemote::setRumble(bool enabled)
{
    int rumble=0;
    if(enabled) rumble=1;
    if (cwiid_set_rumble(wiimote, rumble)) {
        fprintf(stderr, "Error setting rumble\n");
    }
}

int main()
{
	CWiidRemote wiimote;
	int ret = wiimote.open();
	
	if(ret!=0) return 1;
	
	while(1)
	{
		usleep(10000);
        printf("====\n");
		auto s = wiimote.getState();
        if(s.buttons==4) wiimote.setRumble(true); 
        else wiimote.setRumble(false);
	}
}
