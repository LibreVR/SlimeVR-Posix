#include "EventHandler.h"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>

EventHandler::EventHandler()
{
}

void EventHandler::HandleEvent(const std::string& event)
{
    std::vector<std::string> spl;
    boost::split(spl, event, boost::is_any_of("\t "),
                   boost::token_compress_on);
    
    if(spl.size()==9 && spl[0]=="updateTracker")
    {
         int index = boost::lexical_cast<int>(spl[1]);
         //a vector3
         double tx = boost::lexical_cast<double>(spl[2]);
         double ty = boost::lexical_cast<double>(spl[3]);
         double tz = boost::lexical_cast<double>(spl[4]);
         //a quaternion
         double rw = boost::lexical_cast<double>(spl[5]);
         double rx = boost::lexical_cast<double>(spl[6]);
         double ry = boost::lexical_cast<double>(spl[7]);
         double rz = boost::lexical_cast<double>(spl[8]);
         printf("EventHandler::HandleEvent@updateTracker %i tx=%f ty=%f tz=%f rw=%f rx=%f ry=%f rz=%f\n",index,tx,ty,tz,rw,rx,ry,rz);
         // Post pose
         #ifdef OPENVR
         //see https://github.com/SlimeVR/SlimeVR-OpenVR-Driver/blob/main/src/TrackerDevice.cpp
         //pose = {tx, ty, tz, rw, rz, ry, rz}
         //GetDriver()->GetDriverHost()->TrackedDevicePoseUpdated(index, pose, sizeof(vr::DriverPose_t));
         //this->last_pose_ = pose;
         #endif
         return;
    }
    printf("EventHandler::HandleEvent %i\n",spl.size());
}
