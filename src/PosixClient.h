#pragma once
#include <string>

class SlimePosixClient
{
    public:
    int sock=0;
    bool Connect();
    std::string GetEvent();
};
