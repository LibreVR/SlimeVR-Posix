#include "PosixClient.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>

static const char* socket_path = "/tmp/slivevr.sock";

bool SlimePosixClient::Connect()
{
    struct sockaddr_un remote;
    int data_len = 0;
    
    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if(sock<0) return false;
    
    remote.sun_family = AF_UNIX;
	strcpy( remote.sun_path, socket_path );
	data_len = strlen(remote.sun_path) + sizeof(remote.sun_family);

	if(connect(sock, (struct sockaddr*)&remote, data_len)<0) return false;
    
    //TODO: get protocol version
    
    return true;
}

std::string SlimePosixClient::GetEvent()
{
    char buffer[1024];
    int data_len = recv(sock, buffer, sizeof(buffer), 0);
    if(data_len>0 && data_len < 1023)
    {
        return buffer;
    }
    return "error";
}

