#include "PosixClient.h"
#include "EventHandler.h"

int main()
{
	SlimePosixClient cl;
    EventHandler ev;
    
    if(cl.Connect()==false)
    {
        printf("connect failed\n");
        return 1;
    }
    
	while(1)
	{
        std::string event = cl.GetEvent();
        if(event=="error") return 1;
        ev.HandleEvent(event);
	}

	return 0;
}
