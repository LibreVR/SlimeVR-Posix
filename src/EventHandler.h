#pragma once
#include <string>
#include <vector>

class EventHandler
{
    public:
    EventHandler();
    void HandleEvent(const std::string& event);
};
