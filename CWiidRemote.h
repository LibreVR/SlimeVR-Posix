#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <bluetooth/bluetooth.h>
#include <cwiid.h>

class CWiidRemote
{
	cwiid_wiimote_t *wiimote;	/* wiimote handle */
	struct cwiid_state state;	/* wiimote state */
	bdaddr_t bdaddr;	        /* bluetooth device address */
public:
    int open();
    const struct cwiid_state& getState();
    void setRumble(bool enabled);
};
